package edu.epam.izhevsk.junit;

import org.hamcrest.CustomMatcher;
import org.junit.Before;
import org.junit.BeforeClass;
import static org.mockito.AdditionalMatchers.*;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import static org.mockito.Mockito.*;



public class PaymentControllerTest {
  @InjectMocks
  AccountService accountService;
  @InjectMocks
  DepositService depositService;

  @Test
  public void testSuccessDeposite() {
    Long userId = Long.valueOf(100);
    Long amount = Long.valueOf(100);
    accountService = mock(AccountService.class);
    depositService = mock(DepositService.class);
    when(accountService.isUserAuthenticated(userId)).thenReturn(true);
    try {
      when(depositService.deposit(amount, userId))
          .thenReturn("For deposit of amount less than hundred transaction (" + userId
              + ") will be successful");
    } catch (InsufficientFundsException e1) {
      e1.printStackTrace();
    }
    try {
      when(depositService.deposit(gt(amount), anyLong()))
          .thenThrow(new InsufficientFundsException());
    } catch (InsufficientFundsException e) {
      e.printStackTrace();
    }
    PaymentController paymentController = new PaymentController(accountService, depositService);
    try {
      paymentController.deposit(Long.valueOf(50), Long.valueOf(100));
    } catch (InsufficientFundsException e) {
      e.printStackTrace();
    }
    verify(accountService).isUserAuthenticated(Long.valueOf(100));
  }

  @Test(expected = SecurityException.class)
  public void testFailedDepositForUnauthenticatedUser() {
    Long userId = Long.valueOf(50);
    Long amount = Long.valueOf(100);
    accountService = mock(AccountService.class);
    depositService = mock(DepositService.class);
    when(accountService.isUserAuthenticated(userId)).thenReturn(true);
    try {
      when(depositService.deposit(amount, userId))
          .thenReturn("For deposit of amount less than hundred transaction (" + userId
              + ") will be successful");
    } catch (InsufficientFundsException e1) {
      e1.printStackTrace();
    }
    try {
      when(depositService.deposit(gt(amount), anyLong()))
          .thenThrow(new InsufficientFundsException());
    } catch (InsufficientFundsException e) {
      e.printStackTrace();
    }
    PaymentController paymentController = new PaymentController(accountService, depositService);
    try {
      paymentController.deposit(Long.valueOf(50), Long.valueOf(130));
    } catch (InsufficientFundsException e) {
      e.printStackTrace();
    }
    verify(accountService).isUserAuthenticated(Long.valueOf(130));
  }

  @Test
  public void testFailedDepositOfLargeAmountExpectException() {
    Long userId = Long.valueOf(100);
    Long amount = Long.valueOf(100);
    accountService = mock(AccountService.class);
    depositService = mock(DepositService.class);
    when(accountService.isUserAuthenticated(userId)).thenReturn(true);
    try {
      when(depositService.deposit(amount, userId))
          .thenReturn("For deposit of amount less than hundred transaction (" + userId
              + ") will be successful");
    } catch (InsufficientFundsException e1) {
      e1.printStackTrace();
    }
    try {
      when(depositService.deposit(gt(amount), anyLong()))
          .thenThrow(new InsufficientFundsException());
    } catch (InsufficientFundsException e) {
      e.printStackTrace();
    }
    PaymentController paymentController = new PaymentController(accountService, depositService);
    try {
      paymentController.deposit(Long.valueOf(130), Long.valueOf(100));
    } catch (InsufficientFundsException e) {
      e.printStackTrace();
    }
    verify(accountService).isUserAuthenticated(Long.valueOf(100));
  }



}
